from datetime import date
from datetime import timedelta
import pickle
import numpy as np
import matplotlib.pyplot as plt
import os

os.chdir('/home/victor/workspace/e14_users')
with open('dump.pkl', mode='rb') as dmp_file:
    memberlist = pickle.load(dmp_file)


##Find lowest date
lowdatedif = 0
lowdatemember = ''
for member in memberlist:
    try:
        timestamp = int(int(member['start'])/1000)
        startdate = date.fromtimestamp(timestamp)
        print(startdate)
        member['startdate'] = startdate
        member['enddate'] = date.fromtimestamp(int(int(member['end'])/1000))
        if (startdate - date.today()).days < lowdatedif:
            lowdatedif = (startdate - date.today()).days
            lowdatemember = member
    except:
        print(member)
        member['startdate'] = None
        member['enddate'] = None
##
# lowdate = lowdatemember['startdate']
# actives = []
# totalsquit = []
# totals = []
# for day in range(lowdatedif,0,1):
#     daytotal = 0
#     total = 0
#     totalquit = 0
#     print(day)
#     for member in memberlist:
#         if (not member['startdate'] is None) and not member['statusname'] == 'PremierFarnell':
#             if (member['startdate'] - date.today()).days <= day:
#                 total += 1
#                 if(member['enddate'] - date.today()).days >= day:
#                     daytotal = daytotal+1
#             if(member['enddate'] - date.today()).days >= day:
#                 totalquit +=1
#     actives.append(daytotal)
#     totals.append(total)
#     totalsquit.append(totalquit)
## Get stacked plot    
lowdate = lowdatemember['startdate']
actives = [0]*-lowdatedif
totals = [0]*-lowdatedif
totalsquit = [0]*-lowdatedif
datedict ={'New Members':[0]*-lowdatedif,
           'Level 1':[0]*-lowdatedif,
          'Level 2':[0]*-lowdatedif,
          'Level 3':[0]*-lowdatedif,
          'Level 4':[0]*-lowdatedif,
          'Level 5':[0]*-lowdatedif,
          'Level 6':[0]*-lowdatedif,
          'Level 7':[0]*-lowdatedif,
          'Level 8':[0]*-lowdatedif,
          'Level 9':[0]*-lowdatedif,
          'Level 10':[0]*-lowdatedif,
          'Level 11':[0]*-lowdatedif,
          'Level 12':[0]*-lowdatedif,
          'Level 13':[0]*-lowdatedif,
          'Level 14':[0]*-lowdatedif,
          'Level 15':[0]*-lowdatedif,
          'PremierFarnell':[0]*-lowdatedif,
          'e14 Expert':[0]*-lowdatedif
        }
for day in range(lowdatedif,0,1):
    daytotal = 0
    total = 0
    totalquit = 0
    print(day)
    for member in memberlist:
        if (not member['startdate'] is None):
            if (member['startdate'] - date.today()).days <= day:
                totals[day+(-lowdatedif)] += 1
                if(member['enddate'] - date.today()).days >= day:
                    actives[day+(-lowdatedif)] +=1
                    datedict[member['statusname']][day+(-lowdatedif)] += 1
            if(member['enddate'] - date.today()).days >= day:
                totalsquit[day+(-lowdatedif)] += 1
os.chdir('/home/victor/workspace/e14_users')
with open('dump_datedikt.pkl', mode='wb') as dmp_file:
    pickle.dump(datedict,dmp_file)
    pickle.dump(actives,dmp_file)
    pickle.dump(totalsquit,dmp_file)
    pickle.dump(totals,dmp_file)
    pickle.dump(lowdate,dmp_file)
    #print(day)
##Read back db if possible
with open('dump_datedikt.pkl', mode='rb') as dmp_file:
    datedict = pickle.load(dmp_file)
    actives= pickle.load(dmp_file)
    totalsquit=pickle.load(dmp_file)
    totals=pickle.load(dmp_file)
    lowdate =pickle.load(dmp_file)
##Number of Farnell Users
FarnellUsers = 0
statuses = {}
for member in memberlist:
    if not member['statusname'] in statuses.keys():
        statuses[member['statusname']] = 1
    else:
        statuses[member['statusname']] += 1
for i in range(1,16):
    level = 'Level '+str(i)
    if level in statuses.keys():
        print(level, end=': ' )
        print(statuses['Level '+str(i)])
##
fig, ax = plt.subplots()
ax.stackplot(range(lowdatedif,0), datedict['Level 15'], 
                                  datedict['Level 14'],
                                  datedict['Level 13'],
                                  datedict['Level 12'],
                                  datedict['Level 11'],
                                  datedict['Level 10'],
                                  datedict['Level 9'],
                                  datedict['Level 8'],
                                  datedict['Level 7'],
                                  datedict['Level 6'],
                                  datedict['Level 5'],
                                  datedict['Level 4'],
                                  datedict['Level 3'],
                                  datedict['Level 2'],
                                  datedict['Level 1'],
                                  datedict['New Members'],
                                  datedict['PremierFarnell'],
                                  datedict['e14 Expert']
                                  )
plt.show()
##earliest members
seniors = []
for member in memberlist:
    if not member['startdate'] == None:
        #print(member['startdate'] -date.today())
        if member['startdate'] < date(2009,8,1) :
            #print(member['name'],end = ' : ')
            #print(member['startdate'])
            seniors.append(member)

            
##plot
yseries = range(lowdatedif,0)
totalzquit = []
offset = len(memberlist)-195
for item in totalsquit:
    totalzquit.append(offset - item)
start = lowdatedif#-365
p = plt.figure()
plt.plot(yseries[start:],actives[start:])
plt.plot(yseries[start:],totals[start:])
plt.plot(yseries[start:],totalzquit[start:])
#markers = [-365,60]
plt.plot([-80],actives[-lowdatedif-80], 'rD')    
plt.plot([-80],totals[-lowdatedif-80], 'rD')     