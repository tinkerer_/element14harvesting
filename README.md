# README #

This repository hosts the code that I made a few years ago to show that element14 has very, very little active members. At the time, they were claiming to have many tens of thousands of members. Although that was true, only a very small subset was really active.

## What did this script do?

This script looked at the 'members' page, a feature in the Jive framework. The script would ask the page to sort by member level, which is a good indicator of the number of actions the member did on the webpage. After that, the next, next, next page is asked. The results are stored, and can then be post processed to get nice graphs showing that only a few contributors actively **do** something.

## RaspberryPi Effect

The steep increase in the number of members in this graph shows how many members joined suddenly when the RPi became available; remember, you had to register at element14 in order to get one.....

![figure_2.png](https://bitbucket.org/repo/ak5554p/images/1091592809-figure_2.png)

## Does it still work?

I honestly don't know. Commit 5f37246 was the latest that was used with version control on my old PC, the newer commits are just files that had been changed, or not even version controlled. This repository is just a backup with info.